#!/bin/bash

clear

row_no=1

while IFS=',' read -a row ; 
do 
	echo "ROW number: $row_no"
	echo "ROW contents: ${row[@]}"
	echo "ROW Length : ${#row[@]} "
	echo "${row[1]}"
	echo -e "\n----------------------------------\n"
	((row_no++))
done < file.csv
