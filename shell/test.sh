#!/bin/bash

sum=0

while IFS=',' read -a row
do
	#echo "${row[1]}"
	[ "${row[1]}" -eq "${row[1]}" ] 2>/dev/null
	#echo $?
	if [ $? -eq 0 ]
	then
		echo "${row[1]} IS number"
		((sum+=${row[1]}))
	fi
done < file.csv

echo "SUM : $sum"
