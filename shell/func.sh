#!/bin/bash

echo $1
echo $2
x=10

declare -r cons=999

myfunc()
{

	echo $1
	echo $1
	echo "X in func : $x"
	x=20
	echo "x in func : $x"
    local x=50
    echo "x in func : $x"
	
	cons=111

	echo "cons in func: $cons"

        	
}

myfunc func_qw func_er

echo "FUNC done x : $x"
echo "cons outside func : $cons"
