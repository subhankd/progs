#!/bin/bash

if [ $# != 2 ]
then
	echo "Usage : $0 x y    -> x and y are num"
elif ! [ "$1" -eq "$1" ] &> /dev/null
then
	echo "Integers only please"
elif ! [ "$2" -eq "$2" ] &> /dev/null
then
    echo "Integers only please"
else
	echo "Sum: $(($1 + $2))"
fi
	
