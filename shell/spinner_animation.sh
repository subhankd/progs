#!/bin/bash

clear
	
spinner=( '|' '/' "--" '\' );

echo "This script creates an spinning animation for 10 secs. "
echo -e "****************************************************** \n\n"
	
spin() #spin function for rotating the array of \ | -- /
{
	while [ 1 ]
	do
		for i in ${spinner[@]};
		do
			echo -ne "\r Work in Progress $i                                   ";
			sleep 0.1;
		done;
	done
}

count() #function to wait for 10 secs while the animation spins
{
  spin &
  pid=$!

  for i in `seq 1 10`
  do
	  sleep 1;
  done
  set +m
  kill $pid 2>&1 > /dev/null
  echo -ne "\r Task Completed.                                             "
  echo " "  
}


count
