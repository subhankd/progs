
###########
x=3
double_func = lambda x: x*2 
print(double_func(x))
###########


##########
nums = list(range(11))

print(nums)

squares = [n**2 for n in nums]
print(squares)

cubes = list(map(lambda n:n**3, nums))

print(cubes)
##########


##########

filter_squares = [n for n in squares if n>50]
print(filter_squares)


filter_squares = list(filter(lambda n:n>30,squares))
print(filter_squares)

##########