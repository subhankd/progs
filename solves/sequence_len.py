#program to return length of max consecutive sequence
# [5, 2, 99, 3, 4, 1, 100] -> max len  = 5 for [1,2,3,4,5]

arr = []
arr = [5, 2, 99, 3, 4, 1, 100]
#arr = [2, 6, 5, 99, 4, 100, 98, 97, 1]


########################################################################
def max_len(arr):

    arr_set = set(arr)
    length = 0
    max_length = 0

    if(len(arr) == 0):
        print("No Elements. Please provide a Valid Array...")
        exit

    for i in range(0, len(arr)):

        if(arr[i] not in arr_set):
            continue

        length = 1
        r_length = 0
        l_length = 0

        right = arr[i]+1
        left = arr[i]-1

        while right in arr_set:
            arr_set.remove(right)
            r_length += 1
            right += 1

        while left in arr_set:
            arr_set.remove(left)
            l_length += 1
            left -= 1
            
        length = length + r_length + l_length

        max_length = max(max_length, length)
        
    return(max_length)
##################################################################

print(f"Length of max consecutive sequence: {max_len(arr)}")